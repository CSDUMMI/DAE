"""
Decentralised Autonomous Elections
Copyright (C) 2021 CSDUMMI <csdummi.misquality@simplelogin.co>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
from Block import Consensus, Block
from typing import Union
import proto.Block_pb2 as pb
import pickle
from functools import reduce

def defaultApplyFn(data, members, payload):
    """Identity apply function.
    """
    return (data, members, APPLYFN)

defaultApplyFnBytes = pickle.dumps(defaultApplyFn)

class State:
    def __init__(self, data, members, applyFn):
        self.data = data
        self.members = members
        self.applyFn = applyFn


class Blockchain:
    """Create, store and load blockchains from
    persistent storage
    """

    def __init__(self, genesis : Block):
        """
        genesis: Genesis Block in the Chain.
        """
        self.genesis = genesis
        self.genesisData = pb.GenesisData()
        self.genesisData.ParseFromString(self.genesis.data)

        assert self.ratified()

        # most recent validated block
        self.tip = genesis

        self.applyFn = pickle.loads(self.genesisData.APPLYFN)
        assert callable(self.applyFn)


        self.blocks : dict[bytes, Block] = {
            self.genesis.bareCID: self.genesis
        }

        self.state : dict[bytes, State] = {
            self.genesis.bareCID: State(self.genesisData.INITIALSTATE, self.genesisData.INITIALMEMBERS, self.genesisData.APPLYFN)
        }



    def ratified(self):
        """Has the genesis block been validated?
        """
        members = self.genesisData.INITIALMEMBERS
        return self.genesis.consensus(members) == Consensus.VALIDATED

    def current():
        """Returns the (Member, State) pair
        of the most recent certified block
        """

        if self.tip.isGenesis():
            return self.state[self.tip.bareCID]

        certifiedBlock = self.blocks[self.tip.ancestor]
        return self.state[certifiedBlock.bareCID]

    def add(self, block):
        """Add a new block to the chain
        and apply the ancestor block with applyFn
        if the block certified said block
        """
        if self.tip.consensus(self.members, descendants = [block]) != Consensus.CERTIFIED:
            return False

        self.tip = block

        # apply tip to blockchain
        current = self.current()
        self.applyFn = current[2]

        (state, members, applyFn_) = current.applyFn(current.data, current.members, block.data)



        return True

    @classmethod
    def create( cls,
                genesis
            ):
        """Create a Blockchain based on GenesisData.
        Blockchain.create(Blockchain.genesisData(..)).

        The genesis Block has still got to be signed by the members
        in order to be validated.
        """

        return cls(genesis)

    @classmethod
    def genesisData(cls,
                    initialMembers,
                    electionTime : int = 3600000, # 1 hour
                    msgTopic : str = "test-dae-msg-topic",
                    blockProposalTopic : str = "test-dae-block-proposal-topic",
                    votesTopic : str = "test-dae-votes-topic",
                    blockTopic : str = "test-dae-block-topic",
                    initialState : bytes = b"",
                    victoryCondition : float = 0.5,
                    applyFn : bytes = defaultApplyFnBytes
                ):
        """TODO: require pubsub topics as positional arguments.
        """
        genesisData = pb.GenesisData()
        genesisData.INITIALMEMBERS[:] = initialMembers
        genesisData.ELECTIONTIME = electionTime
        genesisData.MSGTOPIC = msgTopic
        genesisData.BLOCKPROPOSALTOPIC = blockProposalTopic
        genesisData.VOTESTOPIC = votesTopic
        genesisData.BLOCKTOPIC = blockTopic
        genesisData.INITIALSTATE = initialState
        genesisData.VICTORYCONDITION = victoryCondition
        genesisData.APPLYFN = applyFn

        return genesisData

    @classmethod
    def createGenesis(cls,
                      genesisData
                     ) -> Block:
        """Create a genesis block based on Genesis data.
        This block is not yet validated - a blockchain cannot
        yet be created based on this Block.
        """
        return Block(genesisData.SerializeToString(), b"")
