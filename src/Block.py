"""
Decentralised Autonomous Elections
Copyright (C) 2021 CSDUMMI <csdummi.misquality@simplelogin.co>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
from Crypto.Hash import SHA3_256
from Crypto.Signature import pkcs1_15
from Crypto.PublicKey import RSA
from functools import reduce
from typing import List, Union
from enum import Enum
import proto.Block_pb2 as pb

class Vote:
    def __init__(self, publicKey, signature, bareCID):
        """Use Vote.create to generate the signature automatically.
        """
        self.publicKey = publicKey
        self.signature = signature
        self.bareCID = bareCID

    def __eq__(self, other):
        return (self.publicKey == other.publicKey
                and self.signature == other.signature
                and self.bareCID == other.bareCID)

    def __str__(self):
        return f"{self.publicKey}\n{self.signature}\n{self.bareCID}"

    def isValid(self, block, members, voters):
        isMember = self.publicKey in members
        alreadyVoted = self.publicKey in map(lambda v: v.publicKey, voters)
        votedForBlock = self.bareCID == block.bareCID
        validSignature = Vote.validSignature(self.publicKey, self.signature, self.bareCID)
        return isMember and not alreadyVoted and votedForBlock and validSignature

    def validSignature(publicKey, signature, bareCID):
        bareCIDHash = SHA3_256.new(bareCID)

        try:
            pkcs1_15.new(RSA.import_key(publicKey)).verify(bareCIDHash, signature)
            return True
        except (ValueError, TypeError):
            return False

    @classmethod
    def create(cls, privateKey, bareCID):
        if type(privateKey) != RSA.RsaKey:
            privateKey = RSA.import_key(privateKey)

        bareCIDHash = SHA3_256.new(bareCID)

        signature = pkcs1_15.new(privateKey).sign(bareCIDHash)

        return cls(privateKey.publickey().export_key("PEM"), signature, bareCID)

    def encode(self):
        vote = pb.Vote()
        vote.publicKey = self.publicKey
        vote.signature = self.signature
        vote.bareCID = self.bareCID

        return vote.SerializeToString()

    @classmethod
    def decode(cls, packet):
        vote = pb.Vote()
        vote.ParseFromString(packet)

        return cls(vote.publicKey, vote.signature, vote.bareCID)

class Consensus(Enum):
    CERTIFIED = 0 # the best
    VALIDATED = 1
    UNCERTAIN = 2
    EMPTY = 3

class Block:
    def __init__(self, data : bytes, ancestor : bytes, votes : List[Vote] = []):
        self.data = data
        self.ancestor = ancestor
        self.votes = votes

        self.bareCID = SHA3_256.new(data + ancestor).digest()

    def __str__(self):
        return f"<Block: {self.data}>"

    def calculateCID(self):
        self.votes.sort(key = lambda v: v.signature) # consistentish ordering of votes.
        self.cid = SHA3_256.new(self.data + self.ancestor + reduce(lambda a, b: a + b, map(lambda v: v.encode(), self.votes), b"")).digest()

    def proposalReady(self, votes):
        """A Block needs to have votes to be proposal ready.
        """
        self.votes = votes
        self.calculateCID()

    def consensus(self, members, descendants = []):
        if self.isCertified(members, descendants):
            return Consensus.CERTIFIED
        elif self.isValid(members):
            return Consensus.VALIDATED
        elif self.votes == []:
            return Consensus.EMPTY
        else:
            return Consensus.UNCERTAIN

    def isCertified(self, members, descendants):
        return any([descendant.isValid(members) for descendant in descendants if descendant.ancestor == self.cid])

    def isValid(self, members):

        def filterValidVotes(voters, vote):
            if vote.isValid(self, members, voters):
                voters.append(vote)

            return voters

        return 0.5*len(members) < len(reduce(filterValidVotes, self.votes, []))

    def isGenesis(self):
        return self.ancestor == b""

    def encode(self):
        block = pb.Block()
        block.data = self.data
        block.ancestor = self.ancestor
        block.votes = [v.encode() for v in self.votes]
        block.bareCID = self.bareCID

        return block.SerializeToString().encode("utf-8")

    @classmethod
    def decode(cls, packet):
        block = pb.Block()
        block.ParseFromString(packet.decode("utf-8"))

        votes = [ Vote.decode(v) for v in block.votes]
        return cls(block.data, block.ancestor, votes)


if __name__ == "__main__":
    print("Creating a new chain...")

    initialMembers = int(input("Number of Initial Members: "))

    members = [RSA.generate(1024) for _ in range(initialMembers)]
    publicMembers = [member.publickey().export_key("PEM") for member in members]

    print(f"{initialMembers} members created")

    genesis = Block(b"Genesis", b"")
    genesis.proposalReady([Vote.create(privkey, genesis.bareCID) for privkey in members])

    print(f"Created genesis {genesis}")

    consensus = genesis.consensus(publicMembers)
    print(f"Genesis consensus is: {consensus}")

    ancestor = genesis

    while True:
        data = input("Add a block: ").encode("utf-8")

        if data == b":quit":
            break

        block = Block(data, ancestor.cid)
        print(f"Created new block {block}")

        votesCount = int(input("How many votes should it receive? "))

        voters = [members[i] for i in range(votesCount % len(members))]

        block.proposalReady([Vote.create(privkey, block.bareCID) for privkey in voters])

        consensus = block.consensus(publicMembers)
        print(f"Consensus: {consensus}")

        print(f"Ancestor of {block} is {ancestor}")
        print(f"Ancestor {ancestor} has consensus: {ancestor.consensus(publicMembers, descendants = [block])}")

        if consensus == Consensus.VALIDATED:
            ancestor = block
        else:
            print(f"Block did not reach a sufficient number of votes. Ancestor remains {ancestor}")
