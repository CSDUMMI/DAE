# DAE

Instead of using a expensive Proof of Work or Proof of Stake
algorithms to create consensus, DAEs use equal elections among
designated members to establish consensus over the network's state.

# Glossary & Language and Definitions
#### DAO
- Decentralized Autonomous Organization: A Blockchain with members.
- DAO: abbreviation of Decentralized Autonomous Organization
- Member: Some person or machine able to vote on elections of the Organization.

#### Definitions of Blockchain
- Election: Block in the Blockchain
- Block: Data shared across the network containing data, votes and ancestor.
- Full Block: Block containing a non-empty field votes.
- Bare Block: Block only containing an empty votes field.
- Blockchain: A single linked list of blocks starting with the genesis block and ending with the last block with consensus.
- Genesis Block: The first block in a blockchain determining the initial members, parameters (identified here by all uppercase) and initial state of the network.
- Tip of a Blockchain: Last certified block in the blockchain
- State of the Network: The custom data stored by the network, curent members and on-chain apply function.

#### Consensus
- Empty Block: A Block without any votes.
- Uncertain Block: A Block that is neither validated nor certified.
- Validated Block: A Block with valid votes fulfilling the victory condition.
- Certified Block: A validated Block with a validated descendant.
- Ratified Blockchain: Blockchain with a validated genesis block.
- Block with consensus: A certified block.
- To apply a block: To change the state of the network based on a block.

#### Block's relationships
- Ancestor Block: ancestor of Block A is the Block a links to with it's ancestor field.
- Descendant Block: descendant of Block A is any block that links to A in it's ancestor.

#### Voting and Validation
- Vote: A message to prove a member voted for a certain bare block to be included in the blockchain.
- Valid Vote: The prove contained in a vote is correct and for the correct block.
- Victory Condition (VICTORYCONDITION): Condition on the Block to be validated. This Condition must only be fulfilled by one block if no voter votes for two different blocks competing for the same place in the chain.
- Absolute Majority Victory: A victory condition that is fulfilled if the absolute majority (>50%) of members votes for it.

#### Consensus mechanisms
- Proof of Work (PoW): Consensus by spending computational power.
- Proof of Stake (PoS): Consensus by spending money.
- Proof of Votes (PoV): Consensus by winning votes. This is the consensus implemented here.

#### Applying a block
- VM: A virtual machine used to execute in a safe and isolated system, that cannot access the machine that the VM is hosted on.
- Apply Function: A function taking the state of the network and a block and returning a new state of the network.
- On chain apply function (APPLYFN): An apply function stored and updated on the blockchain. This allows for Blockchains to govern how they change their networks state.

#### Networking
- Peer-to-Peer network: Network of peers creating a shared state without authoritative peers.
- Light Peer: A machine with very limited computational, financial and networking resources  participating in a Peer-to-Peer network by storing the last state of the network.
- Replicated across the network: Data is shared with some of the peers of the peer-to-peer network.
- Public Key: The public part of a public-private key pair used for asymmetric encryption.
- Private Key: The private part of a public-private key pair used for asymmetric encryption.
- CID Hash Table: A Table from CIDs to their contents.
- DHT: a distributed hash table.

#### Peer Types and Messages
- Messages: Data to be included in a block as part of data.
- Message type: Messages of a certain format and causing a certain change to the network state if applied.
- Validate messages: Each message type can be validated separately based on the current network state.
- Validator: A peer listening for new blocks and validating and checking their certificate before applying them to their local copy of the network state.
- Proposer: A peer listening for new messages and creating blocks based on those messages. If messages are validated based on the current state of the network, proposers have to also be validators.
- Creator: A peer creating a new decentralized autonomous organization.
- Verifier: A peer listening only for two kinds of blocks: the blocks changing members of the organization and blocks they need to initiate actions.

#### Foreign Services
- Foreign Service: Some Peer listening for certified blocks and executing some action based on it. (i.e. a smart contract creating a transaction of a Cryptocurrency when a Block is certified)
- Foreign Transaction: A Message in a Block causing some action on a foreign service.

#### PubSub
- PubSub: Publish - Subscribe Networking Model
- PubSub Topic: A topic to publish messages in and subscribe to, to receive messages.

#### Genesis Parameters
- Public Keys of the initial members (INITIALMEMBERS).
- Election Time (ELECTIONTIME): Milliseconds between adding blocks to the blockchain.
- Messages Topic (MSGTOPIC): PubSub Topic to publish and subscribe to messages in to be included in the next block.
- Block proposals topic (BLOCKPROPOSALTOPIC): PubSub Topic to publish a bare block for members to vote.
- Votes topic (VOTESTOPIC): PubSub Topic to publish votes for bare blocks proposed in Block proposal topic.
- Blocks Topic (BLOCKTOPIC): PubSub Topic to publish and subscribe to newly proposed full blocks.
- Initial Data (INITIALDATA): The data of the network at the begin of the network.
- A secure victory condition (VICTORYCONDITION) is: votes > VICTORYCONDITION(members-length) where 0.5 =< VICTORYCONDITION =< 1.
- The initial apply function (APPLYFN) is also a genesis parameter.

# Discussion
Using these terms you can create a decentralized autonomous organization
without requiring the spending of excessive computational power or money
on a peer-to-peer network of light peers.

To create such a network a peer-to-peer network has to follow this algorithm:
1. Genesis Block is created defining the initial members and parameters.
2. Validators and Proposers are setup and join BLOCKTOPIC, BLOCKPROPOSALTOPIC and MSGTOPIC.
3. Peers create messages and publish them on MSGTOPIC.
4. Proposers gather, validate and compose messages received into blocks.
5. After ELECTIONTIME seconds proposers publish a bare block to BLOCKPROPOSALTOPIC.
6. Members vote on proposed blocks.
7. Validators gather the valid votes in VOTESTOPIC until a Block achieves the victory condition.
8. Once a Block reaches the victory condition Validators publish the full block with all the valid votes in BLOCKTOPIC.
9. The block that won in the previous step also certifies it's ancestor.
10. Validators apply the newly certified block to their network state.
11. Repeat at 4.

## Algorithms for the individual peers
These are extractions of the algorithm above for each of the individual types of peers.

### Creator
To create an organization a creator has to:
1. Get:
  - list of initial member's public keys.
  - parameters of the DAO.
  - initial state
2. Create the Genesis Block.
3. Publish the Genesis Block to other peers. (If you don't have any publish it on a global peers pubsub topic).

### Validator
1. Get
  - Parameters from Genesis
  - Member's public keys from Genesis
  - Initial state from Genesis
2. Subscribe to BLOCKTOPIC and BLOCKPROPOSALTOPIC and VOTESTOPIC
3. If a BLOCKTOPIC message is received.
  1. Validate message.
  2. fetch block.
  3. validate block.
  4. apply validated ancestor of new validated block.
4. If a BLOCKPROPOSALTOPIC message is received.
  1. Validate message.
  2. Fetch bare block.
  3. Check if a bare block has among is ancestor or ancestor's ancestor ... a certified block.
  4. Store the bare block in an intermediate storage.
5. If a VOTESTOPIC message is received.
  1. Validate message.
  2. Fetch vote.
  3. Assert that a block for this vote exists in intermediate storage.
  4. Verify vote.
  5. Add vote to intermediate storage associated with the block
