FROM python:3.7-alpine
COPY . /block
WORKDIR /block

RUN apk add --no-cache gcc linux-headers musl-dev
RUN pip install -r requirements.txt
