"""Tests for Block.py
"""

from Block import Block, Consensus, Vote
from Crypto.PublicKey import RSA

def test_vote():
    privkey = RSA.generate(1024)
    bareCID = b""

    vote = Vote.create(privkey.export_key("PEM"), bareCID)

    assert Vote.validSignature(privkey.publickey().export_key("PEM"),  vote.signature, bareCID)

    packet = vote.encode()
    vote_ = Vote.decode(packet)

    assert type(packet) == bytes
    assert vote_ == vote

    assert type(vote.publicKey) == bytes
    assert type(vote.signature) == bytes
    assert type(vote.bareCID) == bytes

    assert type(vote_.publicKey) == bytes
    assert type(vote_.signature) == bytes
    assert type(vote_.bareCID) == bytes

def test_create_blocks():
    members = [RSA.generate(1024) for _ in range(5)]
    publicMembers = [member.publickey().export_key("PEM") for member in members]

    block = Block(b"Genesis", b"")
    ancestor = None

    for i in range(200):
        assert block.consensus(publicMembers) == Consensus.EMPTY

        votes = [Vote.create(privkey, block.bareCID) for privkey in members]

        block.proposalReady(votes[:2])
        assert block.consensus(publicMembers) == Consensus.UNCERTAIN

        block.proposalReady(votes)
        assert block.consensus(publicMembers) == Consensus.VALIDATED

        if ancestor is not None:
            assert ancestor.consensus(publicMembers, descendants = [block]) == Consensus.CERTIFIED

        ancestor = block
        block = Block(bytes(i), ancestor.cid)
