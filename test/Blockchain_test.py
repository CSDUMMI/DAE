"""Tests for ../src/Blockchain.py
"""
from Blockchain import Blockchain
from Block import Block, Vote
from Crypto.PublicKey import RSA
import pickle

def sample_apply(data, members, payload):
    """Create increasingly longer blockchain data
    """
    return (data + payload, members, sample_apply)

def test_create_chain():
    members = [RSA.generate(1024) for _ in range(5)]
    publicMembers = [member.publickey().export_key("PEM") for member in members]

    genesisData = Blockchain.genesisData(publicMembers, victoryCondition = 0.5, applyFn = pickle.dumps(sample_apply))
    genesis = Blockchain.createGenesis(genesisData)

    genesis.proposalReady([Vote.create(privkey, genesis.bareCID) for privkey in members])

    chain = Blockchain.create(genesis)

    assert chain.applyFn == sample_apply
